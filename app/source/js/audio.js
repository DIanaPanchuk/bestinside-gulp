$(function(){
	$('.sound-button').on('click', function() {
		$(this).toggleClass('open');
	});
});

$(document).on('mouseenter', '.audio-action', function(event) {
	var $that = $(this);
	var id = $that.data('hover-audio');
	var audioObject = document.getElementById(id);
	audioObject.play();
});

$(document).on('click', '.audio-action', function(event) {
	var $that = $(this);
	var id = $that.data('click-audio');
	if(id) {
		var audioObject = document.getElementById(id);
		audioObject.play();
	}
});

$(document).on('click', '.audio-toggle-action', function(event) {
	var $that = $(this);
	var enabledClass = $that.data('toggle-class');
	var id, audioObject;
	if( $that.hasClass(enabledClass) ) {
		console.log('audio-on');
		id = $that.data('on-audio');
		audioObject = document.getElementById(id);
		mutedSound(false);
	} else {
		id = $that.data('off-audio');
		audioObject = document.getElementById(id);
		mutedSound(true);
	}
	if(id) audioObject.play();
	
});

function mutedSound(muted) {
	var $audioContainer = $('.audio-action-container');
	var audioArrayId = [];

	$audioContainer.find('audio').each(function () {
		if ( $(this).attr('id') ) audioArrayId.push($(this).attr('id'));
	});

	audioArrayId.forEach( function (audioElement) {
		var audio = document.getElementById(audioElement);
		audio.muted = muted;
	});
}

$.fn.equalizerAnimation = function(speed) {
	var $equalizer = $(this);
	setInterval(function () {
		$equalizer.find('span').eq(0).css({height: randomBetween(15, 20) + 'px'});
		$equalizer.find('span').eq(1).css({height: randomBetween(10, 30) + 'px'});
		$equalizer.find('span').eq(2).css({height: randomBetween(12, 35) + 'px'});
		$equalizer.find('span').eq(3).css({height: randomBetween(20, 27) + 'px'});
		$equalizer.find('span').eq(4).css({height: randomBetween(16, 25) + 'px'});
	}, speed);
	$(document).on('click', '.js-equalizer', function() {
		$equalizer.toggleClass('is-paused');
	});
}

$(document).ready(function() {
	$('.js-equalizer').equalizerAnimation(180);
});

function randomBetween(min, max) {
	if (min < 0) {
		return min + Math.random() * (Math.abs(min)+max);
	} else {
		return min + Math.random() * max;
	}
}