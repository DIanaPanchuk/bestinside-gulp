var $grid,
	colors = ['lilac', 'pink', 'green'],
	per_page = 15, page = 1, pages,
	category = "all",
	filterAnim,
	popUpAction;
function getTemplate(data) {
	var els = [];
	$.each(data, function(i, el){
		var el ='<div class="grid-item new ' + el.category + ' ' + getColor(el.id) + '" data-category="' + el.category + '" data-photo="' + el.usePhoto + '" data-id="' + el.id + '">'
			+ '<div class="item-wrap">'
				+ '<div class="item">'
					+ '<img src="' + el.imageBig + '" />'
					+ '<div class="mask desc">'
						+ '<h2>' + el.name + '</h2>'
						+ '<p>' + el.shortDesc + '</p>'
						+ '<p class="full-desc hidden">' + el.fullDesc + '</p>'
						+ '<div class="info">'
							+ '<span>#найкращевсередині</span>'
							+ '<div class="social">'
								+ '<button class="icon fb audio-action js-share-fb" data-link="http://www.mcdonalds.ua/bestinside/" '
								+ 'data-title="'+ el.name +'" data-img-share="'+ el.imageBig +'" '
								+ 'data-text="'+ el.shortDesc +'" data-caption="'+ el.name +'" '
								+ 'data-hover-audio="iconShareHover" data-click-audio="iconShareClick"></button> '
								+ '<button class="icon vk audio-action js-share-vk"  data-link="http://www.mcdonalds.ua/bestinside/" '
								+ 'data-title="'+ el.name +'" data-img-share="'+ el.imageBig +'" '
								+ 'data-text="'+ el.shortDesc +'" '
								+ 'data-hover-audio="iconShareHover" data-click-audio="iconShareClick"></button> '
							+ '</div>'
						+ ' </div>'
					+ '</div>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		els.push(el);
	});
	return els;
}

function getData(category, page) {
	$.ajax({
		url: 'http://www.mcdonalds.ua/bestinsideapi/post?per_page=' + per_page + '&page=' + page + '&section=' + category,
		success: function(resp) {

			if (resp.meta.pages == 0) {
				$grid.packery('layout');
				$('.empty').removeClass('hidden');
			} else {
				$('.empty').addClass('hidden');
			}

			$grid.append(getTemplate(resp.posts));
			var $newItems = $('.grid-item.new');

			$grid.packery( 'appended', $newItems );

			$newItems.imagesLoaded().always( function() {
				$grid.packery('layout');
				animIn($newItems);
			});

			initFilterAnimation();

			pages = resp.meta.pages;
			$('.preloader-wrap').addClass('hidden');
		}
	});
}
function initGrid() {
	$grid = $('.grid').packery({
			itemSelector: '.grid-item',
			transitionDuration: '0'
		});

	$grid.imagesLoaded().progress( function() {
		$grid.packery('layout');
	});
	console.log('grid init');
	addEvents($('.grid-item'));
}
function showPopUp() {
	$('.item-full').addClass('open');
	$('html').addClass('ovfHid');
	popUpAction.play();
}

function addEvents(els) {

	els.on('click', function(){
		if ($(document).width() >= 750) {
			var color = getColor($(this).data('id'));
			var category = $(this).data('category');
			var img = $(this).find('img').attr('src');
			var title = $(this).find('h2').text();
			var desc = $(this).find('.full-desc').text();
			var hashTag = $(this).find('.info span').text();

			if ($(this).data('photo') == false) {
				var photo = 'no-photo';
			} else {
				var photo = "";
			}

			$('.item-full').removeClass('all tasty happy love bright lilac pink green no-photo');
			$('.item-full').addClass(category + ' ' + color + ' ' + photo);
			$('.item-full').find('img').attr('src', img);
			$('.item-full').find('h2').text(title);
			$('.item-full').find('p').text(desc);
			$('.item-full').find('.info span').text(hashTag);
			$('.item-full').find('.js-share-fb').attr({
				'data-link': 'http://www.mcdonalds.ua/bestinside/',
				'data-title': title,
				'data-img': img,
				'data-text': desc,
				'data-caption': title,
			});
			$('.item-full').find('.js-share-vk').attr({
				'data-link': 'http://www.mcdonalds.ua/bestinside/',
				'data-title': title,
				'data-img': img,
				'data-text': desc
			});
			showPopUp();
		} else {
			return;
		}
	});
}
var resize =  function() {
	$grid.packery('layout');
	console.log('resize done');
}
function initFilterAnimation() {
	filterAnim = TweenMax.to($('.grid-item'), 1, {scale:0, autoAlpha: 0, paused:true}, 0.3);
	// filterAnim.play();
	// filterAnim.reverse();
}
function popUpAnimationInit() {
	var _leftPanel = $('.left-panel');
	var leftElements = _leftPanel.find('.image');

	var _rightPanel = $('.right-panel');
	var rightElements = _rightPanel.find('.burger-icon, h2, p, .info');

	popUpAction = new TimelineMax({
		paused: true,
		onComplete: function() {
			$(_leftPanel).css({'transform': 'translate(0%, 0%) translate3d(0px, 0px, 0px)'});
			leftElements.css({'transform': 'translate(0%, 0%) translate3d(0px, 0px, 0px)'});

			$(_rightPanel).css({'transform': 'translate(0%, 0%) translate3d(0px, 0px, 0px)'});
			$.each(rightElements, function(){
				$(this).css({'transform': 'translate(0%, 0%) translate3d(0px, 0px, 0px)'});
			});
		},
		onReverseComplete: function() {
			$('.item-full').removeClass('open');
			$('html').removeClass('ovfHid');

			$(_leftPanel).css({'transform': 'translate(-100%, 0%) translate3d(0px, 0px, 0px)'});
			leftElements.css({'transform': 'translate(-100%, 0%) translate3d(0px, 0px, 0px)'});

			$(_rightPanel).css({'transform': 'translate(100%, 0%) translate3d(0px, 0px, 0px)'});
			$.each(rightElements, function(){
				$(this).css({'transform': 'translate(100%, 0%) translate3d(0px, 0px, 0px)'});
			});
		}
	});


	popUpAction.to(_leftPanel, 1.5, {
		xPercent: 100,
		autoAlpha: 1,
		force3D: true,
		ease: Expo.easeOut
	})

	popUpAction.staggerTo(leftElements, 1.5, {
		xPercent: 100,
		autoAlpha: 1,
		force3D: true,
		ease: Expo.easeOut
	}, 0.1, "-=1.5")

	popUpAction.to(_rightPanel, 1.5, {
		xPercent: -100,
		autoAlpha: 1,
		force3D: true,
		ease: Expo.easeOut
	}, "-=1.5")

	popUpAction.staggerTo(rightElements, 1.5, {
		xPercent: -100,
		autoAlpha: 1,
		force3D: true,
		ease: Expo.easeOut
	}, 0.1, "-=1.5");

	popUpAction.to('.close', 0.5, {
		autoAlpha: 1,
		scale: 1,
		delay: 1
	}, 0, "-=1.5");
}
function animIn(items) {
	console.log(items);
	TweenMax.set($('.grid-item.new'), {
		opacity: 0,
		y: 100
	});

	TweenMax.staggerTo($('.grid-item.new'), 1.5, {
		y: 0,
		opacity: 1,
		force3D: true,
		ease: Expo.easeOut,
		onComplete: function() {
			$.each(items, function(){
				addEvents($(this));
				$(this).removeClass('new');
			});
			console.log('in anim done');
		}
	}, 0.2);
}
$(window).on('resize', function() {
	resize();
});
function getColor(i) {
	if (i >= 0 && i <= 2) {
		return colors[i];
	} else if (i%3 == 0) {
		return colors[0];
	} else if (i%3 == 1) {
		return colors[1];
	} else if (i%3 == 2) {
		return colors[2];
	}
}
$(function(){
	popUpAnimationInit();
	// initGrid();
	// getData(category, page);

	var win = $(window);
	win.scroll(function() {
		if ($(document).height() - win.height() == win.scrollTop()) {
			page++;
			console.log(page);
			console.log(pages);
			if (page <= pages) {
				$('.preloader-wrap').removeClass('hidden');
				getData(category, page);
			} else {
				$('.preloader-wrap').addClass('hidden');
				console.log('no more items');
			}

		}
		if (win.scrollTop() > 500 && $(window).width() < 970 ) {
			$('.js-move-top').removeClass('invisible');
			console.log('show rocket on mobile');
		} else if (win.scrollTop() > 100) {
			$('.js-move-top').removeClass('invisible');
			console.log('show rocket desktop');
		}

	});
	// show rocket for mobiles
	// $(win).on('scrollstart', function(){
	// 	if (win.scrollTop() > 100) {
	// 		$('.js-move-top').removeClass('invisible');
	// 	}
	// });
	$('.filter').on('click', '.js-gallery-filter', function() {
		$('.show-filter, .filter').removeClass('open');
		$('.empty').addClass('hidden');
		$('.filter li').removeClass('active');
		$(this).addClass('active');



		category = $(this).data('category');
		page = 1;

		filterAnim.play().eventCallback("onComplete", function() {
			$('.preloader-wrap').removeClass('hidden');
			$grid.packery( 'remove', $('.grid-item'));
			$grid.packery('layout');
			getData(category, page);
		});

	});
	$('.close, .left-panel, .right-panel').on('click', function() {
		popUpAction.reverse();
	});

	$('.show-filter').on('click', function(){
		$(this).toggleClass('open');
		$('.filter').toggleClass('open');
	});

	$('.js-move-top').on('click', function(e) {
		/* rocket move */
		var rocket = $(this).children('i');
		rocket.addClass('running');

		setTimeout(function(){
			rocket.removeClass('running');
		}, 2000);

		/* material click */
		var ink, d, x, y;
		if($(this).children('i').find(".ink").length === 0){
			$(this).children('i').prepend("<strong class='ink'></strong>");
		}

		ink = $(this).children('i').find(".ink");
		ink.removeClass("animate");

		console.log(ink);

		if(!ink.height() && !ink.width()){
			d = 10;
			ink.css({height: d, width: d});
		}

		x = e.pageX - $(this).children('i').offset().left - ink.width()/2;
		y = e.pageY - $(this).children('i').offset().top - ink.height()/2;

		ink.css({top: y+'px', left: x+'px'}).addClass("animate");

		$(window.opera ? 'html' : 'html, body').animate({
			scrollTop: 0
		}, 1000); // scroll takes 1 second

		setTimeout(function(){
			$('.js-move-top').addClass('invisible');
		}, 1500);

	});
});