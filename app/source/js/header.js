$(document).ready(function(){
  /* shake */
  var randomDelay;
  $('.shake').each(function(indx, el) {
    randomDelay = Math.floor((Math.random() * 100) + 1) / 100;
    $(this).css({
      '-webkit-animation-delay':randomDelay + 's',
      'animation-delay':randomDelay + 's'
    });
  });
});