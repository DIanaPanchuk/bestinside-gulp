$(document).ready(function(){
  $('.js-digits').animateNumber({
    number: 100,
    numberStep: function(now, _this) {
      var _this = $(_this.elem);
      var number = Math.ceil(now);

      if(number === 30) {
        $('.js-slogan').addClass('show');
      }

      _this
        .prop('number', number) // keep current prop value
        .text(number);
    },
  },
  1000, function() {
    //hand catch the sandwich
    $('.js-hand').addClass('catch');
    //digits fadeout
    $('.js-digits').addClass('loaded');
    //sandwich move with hand
    $('.js-sandwich').addClass('takeout');

    setTimeout(function(){
      //remove slogan
      $('.js-slogan').removeClass('show');
    }, 2000);

    setTimeout(function(){
      //preloader violet bg fadeout
      $('.preloader-wrapper').addClass('loaded');
      //start animation of #mainBurger
      $('#mainBurger').attr('src', 'i/burger-anim.gif');
      //add backgroun for header/main
      $('.js-header, .js-content-main').addClass('bg-anim');
    }, 2500);

    setTimeout(function(){
      //join header elements
      $('.js-header-item').removeClass('waiting');
      //join main elements
      $('.js-main-item').each(function(indx, el){
        $(el).css('transition-delay', indx*240/1000 + 's');
        $(el).removeClass('waiting');
      });

    }, 3250);
    setTimeout(function(){
      $('.scroll-wrap').addClass('show');
    }, 5000);
  });
});