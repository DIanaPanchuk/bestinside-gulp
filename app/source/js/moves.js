$(function(){
	var $pages = $('.home, .gallery')
		isAnimating = false,
		current = 0,
		endCurrPage = false,
		endNextPage = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		support = Modernizr.cssanimations,
		allowSwitchToMain = false,
		allowSwitchToGallery = true,
		galleryIsLoad = false;

	$('.js-to-main').on('click', function() {

		if ($('body').attr('data-active') != 'main') {
			// move to main page (from top to bottom)

			if ($('.full-content').hasClass('move') ) {
				closeRules();
				setTimeout(function(){
					toMain();
				}, 1000);
			} else {
				toMain();
			}
			allowSwitchToGallery = true;
			allowSwitchToMain = false;
		} else {
			return false;
		}


	});

	$(window).on('mousewheel', _.debounce(function(event) {

		if (event.deltaY > 0 && allowSwitchToMain) {
			// move to main page (from top to bottom)
			if ($('.full-content').hasClass('move') ) {
				closeRules();
				setTimeout(function(){
					toMain();
				}, 1000);
			} else {
				toMain();
			}

		} else if (event.deltaY < 0 && allowSwitchToGallery) {
			// move to gallery page (from bottom to top)
			if ($('.full-content').hasClass('move') ) {
				closeRules();
				setTimeout(function(){
					toGallery();
				}, 1000);
			} else {
				toGallery();
			}
		}
	}, 100));

	// swipe events
	$(".home ").swipe( {
		swipeUp: function(event) {
			if (allowSwitchToGallery) {
				// move to gallery page (from bottom to top)
				if ($('.full-content').hasClass('move') ) {
					closeRules();
					setTimeout(function(){
						toGallery();
					}, 1000);
				} else {
					toGallery();
				}
				console.log('swipe top');
			}
		},
		//Default is 75px, set to 0 for demo so any distance triggers swipe
		threshold: 0
	});

	$(".rules ").swipe( {
		swipeUp: function () {
			closeRules();
		}
	} );
	// swipe events end

	function toMain() {
		$(window).scrollTop(0); // fix problem when scroll top more than 0
		switchScreen('1','0','down');
		$('body').attr('data-active', 'main');
		$('html').addClass('ovfHid');
		$('.js-header-slogan').addClass('waiting');
		allowSwitchToGallery = true;
		allowSwitchToMain = false;
	}
	function toGallery() {
		switchScreen('0','1','up');
		$('body').attr('data-active', 'gallery');
		$('html').removeClass('ovfHid');
		$('.js-header').removeClass('bg-anim').addClass('bg-static');
		$('.js-header-slogan').removeClass('waiting');
		allowSwitchToMain = false;
		allowSwitchToGallery = false;

		// load gallery if we go there first time
		if (!galleryIsLoad) {
			galleryIsLoad = true;
			initGrid();
			getData(category, page);
		}

	}

	function closeRules() {
		// if ($('.full-content').hasClass('move') ) {
			$('.full-content').removeClass('move');
			$('.full-content').css({
				'-webkit-transform':'translate(0,0)',
				'transform':'translate(0,0)'
			});

			$('.js-rules').addClass('waiting');
			$('.js-rules').addClass('trs-del-0');
		// }
	}

	function switchScreen(fromPage, toPage, direction) {
		// var currentPage = fromPage,
			// nextPage = toPage;
		console.log('current page is animating ' + isAnimating);
		if( isAnimating ) {
			return false;
		}
		isAnimating = true;


		var $currPage = $pages.eq(fromPage);
		var $nextPage = $pages.eq(toPage).addClass( 'pt-page-current' ),
						outClass = '', inClass = '';

		switch( direction ) {
			case 'up':
				outClass = 'pt-page-fade';
				inClass = 'pt-page-moveFromBottom pt-page-ontop';
				break;
			case 'down':
				outClass = 'pt-page-fade';
				inClass = 'pt-page-moveFromTop pt-page-ontop';
				break;
		}


		$currPage.addClass( outClass ).on( animEndEventName, function() {
			console.log('current anim ended');
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			console.log('next anim ended');
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		});

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}
	}

	function init() {
		$pages.each( function() {
			var $page = $(this);
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		});
		$pages.eq( current ).addClass( 'pt-page-current' );
	}

	init();

	function onEndAnimation( $outpage, $inpage ) {
		console.log('onEndAnimation function');
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
		isAnimating = false;
	}
	function resetPage( $outpage, $inpage ) {
		console.log($outpage.data( 'originalClassList' ));
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' pt-page-current' );
		if ($('body').attr('data-active') == 'main') {
			$('.js-header').addClass('bg-anim').removeClass('bg-static');
			$('.js-content-main').addClass('bg-anim');
		}
	}
});