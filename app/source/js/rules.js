$(function(){
	$('.rules-slider').lightSlider({
		item: 3,
		enableTouch: false,
		enableDrag: false,
		freeMove: false,
		slideMargin: 0,
		responsive : [
			{
				breakpoint: 479,
				settings: {
					item: 1,
					slideMove: 1,
					slideMargin: 0,
					enableTouch: true,
					enableDrag: true,
					freeMove: true,
				}
			}
		],
		onSliderLoad: function() {
			$('.lSPrev').addClass('hidden');
		},
		onAfterSlide: function(el) {
			var current = el.getCurrentSlideCount();
			if (current == $('.rules-slider .box').length) {
				$('.lSNext').addClass('hidden');
			} else {
				$('.lSNext').removeClass('hidden');
			}
			if (current == 1) {
				$('.lSPrev').addClass('hidden');
			} else {
				$('.lSPrev').removeClass('hidden');
			}
		}
	});

	/* show rules */
	$('.js-toggle-rules').on('click', function(e){
		e.preventDefault();

		var fullContent = $('.full-content');

		if(fullContent.hasClass('move')) {
			fullContent.removeClass('move');
			fullContent.css({
				'-webkit-transform':'translate(0,0)',
				'transform':'translate(0,0)'
			});

			$('.js-rules').addClass('waiting');
			$('.js-rules').addClass('trs-del-0');
		} else {
			var moveHeight = $('.rules').height()+$('.header').outerHeight();
			fullContent.addClass('move');
			fullContent.css({
				"-webkit-transform" : "translate(0, "+ moveHeight +"px)",
				"transform" : "translate(0, "+ moveHeight +"px)"
			});

			$('.js-rules').removeClass('waiting');
			$('.js-rules').removeClass('trs-del-0');
		}
	});
});