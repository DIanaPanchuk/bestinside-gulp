;$(function() {
	$('.btn-share')
		.on('mouseenter', function(e) {
				var parentOffset = $(this).offset(),
				relX = e.pageX - parentOffset.left,
				relY = e.pageY - parentOffset.top;
				$(this).find('span').css({top:relY, left:relX})
		})
		.on('mouseout', function(e) {
				var parentOffset = $(this).offset(),
				relX = e.pageX - parentOffset.left,
				relY = e.pageY - parentOffset.top;
			$(this).find('span').css({top:relY, left:relX});
		});

	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function() {
		FB.init({
			appId: '1699492566990496',
			xfbml: true,
			version: 'v2.6'
		});
	});

	$(document).on('click', '.js-share-fb', function(event) {
		event.preventDefault();
		var $that = $(this);
		Share.facebook(
			$that.attr('data-link'), 
			$that.attr('data-title'), 
			$that.attr('data-img-share'), 
			$that.attr('data-text'), 
			$that.attr('data-img-caption') 
		);
	});

	$(document).on('click', '.js-share-vk', function(event) {
		event.preventDefault();
		var $that = $(this);
		Share.vkontakte(
			$that.attr('data-link'), 
			$that.attr('data-title'), 
			$that.attr('data-img-share'), 
			$that.attr('data-text')
		);
	});
});

var Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		var url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},

	/**
	 * @param  {[type String] посилання на яке буде вести пост}
	 * @param  {[type String] заголовок поста}
	 * @param  {[type String] картинка поста}
	 * @param  {[type String] основний текст поста}
	 * @param  {[type String] підпис під постом}
	 * @return {[type Function] callback з результатами шерінгу}
	 */
	facebook: function(purl, ptitle, pimg, text, caption) {
		debugger
		FB.ui({
			method:      'feed',
			link:        purl,
			caption:     caption,
			picture:     pimg,
			description: text,
			name:        ptitle

		}, function (response) {
			console.log(response);
		});
	},

	popup: function(url) {
		window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
	}
};